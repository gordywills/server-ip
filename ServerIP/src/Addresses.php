<?php

namespace ServerIP;

use Leth\IPAddress\IP;
use Leth\IPAddress\IPv4;
use Leth\IPAddress\IPv6;

class Addresses
{
    /**
     * @var IPv4\Address
     */
    private $localIP4 = null;
    /**
     * @var IPv6\Address
     */
    private $localIP6 = null;
    /**
     * @var IP\Address
     */
    private $externalIP = null;

    /**
     * Auto populate the IP values on construction
     */
    public function __construct()
    {
        $this->refreshIPs();
    }

    /**
     * Populate the values of the IP addresses
     */
    public function refreshIPs()
    {
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            $ip4List = gethostbynamel(gethostname());
            $ip6List = [];
        } else {
            $ip4s = `ip addr show | grep inet | grep -v inet6 | awk '{print $2}' | cut -d/ -f1`;
            $ip6s = `ip addr show | grep inet6 | awk '{print $2}' | cut -d/ -f1`;
            $ip4List = explode("\n", $ip4s);
            $ip6List = explode("\n", $ip6s);
        }
        $this->setLocalIP4($ip4List);
        $this->setLocalIP6($ip6List);
        $this->setExternalIP();
    }

    /**
     * set the local IPv4 address as the first non loopback address
     * default to the loop back address where no other exists
     *
     * @param array $ip4List an array of IP4 addresses
     */
    private function setLocalIP4($ip4List)
    {
        foreach ($ip4List as $ip4) {
            if ($ip4 === '127.0.0.1') {
                continue;
            }
            if ($ip4 !== '') {
                $this->localIP4 = IPv4\Address::factory($ip4);
                break;
            }
        }
        if ($this->localIP4 === null) {
            $this->localIP4 = IPv4\Address::factory('127.0.0.1');
        }
    }

    /**
     * set the local IPv6 address as the first non loopback address
     * default to the loop back address where no other exists
     *
     * @param array $ip6List an array of IP6 addresses
     */
    private function setLocalIP6($ip6List)
    {
        foreach ($ip6List as $ip6) {
            if ($ip6 === '::1') {
                continue;
            }
            if ($ip6 !== '') {
                $this->localIP6 = IPv6\Address::factory($ip6);
                break;
            }
        }
        if ($this->localIP6 === null) {
            $this->localIP6 = IPv6\Address::factory('::1');
        }
    }

    /**
     * set the external address from an internet resource
     * default to the loop back address where either the site is down or there is no connectivity
     */
    private function setExternalIP()
    {
        $headers = get_headers('http://api.ipify.org');
        $code = substr($headers[0], 9, 3);
        if ($code != "200") {
            $extIP = '127.0.0.1';
        } else {
            $extIP = file_get_contents('http://api.ipify.org');
        }
        $this->externalIP = IP\Address::factory($extIP);
    }

    /**
     * get the human readable version of the IP string
     *
     * @return string
     */
    public function getLocalIP4String()
    {
        if (!($this->getLocalIP4() instanceof IP\Address)) {
            return '';
        }

        return trim($this->getLocalIP4()->format(IP\Address::FORMAT_COMPACT));

    }

    /**
     * Get a representation of of the IP address
     *
     * @return IP\Address
     */
    public function getLocalIP4()
    {
        return $this->localIP4;
    }

    /**
     * get the human readable version of the IP string
     *
     * @return string
     */
    public function getLocalIP6String()
    {
        if (!($this->getLocalIP6() instanceof IP\Address)) {
            return '';
        }

        return trim($this->getLocalIP6()->format(IP\Address::FORMAT_COMPACT));
    }

    /**
     * Get a representation of of the IP address
     *
     * @return IP\Address
     */
    public function getLocalIP6()
    {
        return $this->localIP6;
    }

    /**
     * Get a representation of of the IP address
     *
     * @return IP\Address
     */
    public function getExternalIP()
    {
        return $this->externalIP;
    }

    /**
     * get the human readable version of the IP string
     *
     * @return string
     */
    public function getExternalIPString()
    {
        if (!($this->externalIP instanceof IP\Address)) {
            return '';
        }

        return $this->externalIP->format(IP\Address::FORMAT_COMPACT);
    }

}