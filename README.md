# ServerIP

## Introduction
This is a little tool I wrote to help with a Raspberry Pi project.  It very simply tries to ascertain values for the loacl IPv4, IPv6 and external (either standard) IP Addresses.  It then hold them together in a single object as IP objects defined in [Leth\PHP-IPAddress](https://github.com/leth/PHP-IPAddress).

This is my very first attempt at putting something in composer, so feed back is welcomed, but be gentle.

## Installation
Using composer is preferred:

Commandline: `php composer.phar require "gordywills/server-ip":"dev-master"`

composer.json:

```json
{
    "require": {
        "gordywills/server-ip": "dev-master"
    }
}
```

## Use
Instantiate a new instance:

```php
<?php
$serverIPAddresses = new ServerIP\Addresses();
```

Then access the address string through the public methods:

```php
<?php
echo $serverIPAddresses->getLocalIP4String();
```

### Public Methods

* `public function getLocalIP4()`   
* `public function getLocalIP6()` Under windows always returns loopback addr  
* `public function getExternalIP()` If no Internet connection available returns local loopback

Get an object representing the IP Address
 
* `public function getLocalIP4String()`
* `public function getLocalIP6String()`
* `public function getExternalIPString()`

Get a string in human readable form

* `public function refreshIPs()`

Update all the IP addresses in case they have changed since the object was created

## Example

```php
<?php
//initiate the composer autoload
require 'vendor/autoload.php';

use ServerIP\Addresses;

$serverIPAddresses = new Addresses();

echo $serverIPAddresses->getLocalIP4String();
echo "\n";
echo $serverIPAddresses->getLocalIP6String();
echo "\n";
echo $serverIPAddresses->getExternalIPString();
echo "\n";

```