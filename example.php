<?php
//initiate the composer autoload
require 'vendor/autoload.php';

use ServerIP\Addresses;

$serverIPAddresses = new Addresses();

echo $serverIPAddresses->getLocalIP4String();
echo "\n";
echo $serverIPAddresses->getLocalIP6String();
echo "\n";
echo $serverIPAddresses->getExternalIPString();
echo "\n";